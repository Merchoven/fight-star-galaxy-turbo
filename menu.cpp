#include <SFML/Graphics.hpp>
#include "player.h"
#include "GameStateEnum.h"
#include "PauseEnum.h"
/*
 * Menu actions - so far, just the action for keypresses on menu
 */

void menuAct(int &gameState, sf::Event &event, sf::Text &start, sf::Text &exit)
{
    if(event.key.code == sf::Keyboard::Escape)
    {
        gameState = CLOSING;
        return;
    }

    if((event.key.code == sf::Keyboard::Down) || (event.key.code == sf::Keyboard::Up))
    {
        if(start.getColor() == sf::Color::White)
        {
            start.setColor(sf::Color::Red);
            exit.setColor(sf::Color::White);
        } else if(start.getColor() == sf::Color::Red)
        {
            start.setColor(sf::Color::White);
            exit.setColor(sf::Color::Red);
        }
    }

    if(event.key.code == sf::Keyboard::Return)
    {
        if(start.getColor() == sf::Color::White)
        {
            gameState = IN_GAME;
        } else if(start.getColor() == sf::Color::Red)
        {
            gameState = CLOSING;
        }
    }
}

void inputHandler(sf::Event input, Player& playerShip, int& paused)
{
    if (input.type == sf::Event::KeyPressed)
    {
        if(input.key.code == sf::Keyboard::Escape)
        {
            paused = PAUSED;
        }
        if(input.key.code == sf::Keyboard::Left)
        {
            playerShip.accel = -1;
        }
        if(input.key.code == sf::Keyboard::Right)
        {
            playerShip.accel = 1;
        }
    } else if (input.type == sf::Event::KeyReleased)
    {
        if((input.key.code == sf::Keyboard::Left) || (input.key.code == sf::Keyboard::Right))
        {
            playerShip.decelerating = true;
            playerShip.accel = 0;
        }
    }
}

void pausedInput(sf::Event input, int& gameState, int& paused, sf::Clock& unpauser)
{
    if(input.key.code == sf::Keyboard::M)
    {
        paused = UNPAUSED;
        gameState = MAIN_MENU;
    }
    if((input.key.code == sf::Keyboard::P) || (input.key.code == sf::Keyboard::Space))
    {
        paused = UNPAUSING;
        unpauser.restart();
    }
}
