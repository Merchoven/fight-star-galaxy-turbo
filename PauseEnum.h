#ifndef PAUSEENUM_H_INCLUDED
#define PAUSEENUM_H_INCLUDED

enum PauseTracker {
        UNPAUSED,
        PAUSED,
        UNPAUSING
};

#endif // PAUSEENUM_H_INCLUDED
