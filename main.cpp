#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include "player.h"
#include "menu.h"
#include "GameStateEnum.h"
#include "PauseEnum.h"
#include "updater.h"
#include "logging_and_debug.h"

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "Galactiga", sf::Style::Titlebar|sf::Style::Close);
    window.setVerticalSyncEnabled(true);
    window.setMouseCursorVisible(false);

    int currentGameState = MAIN_MENU;
    int paused = UNPAUSED;

    //Making the main menu
    sf::Font Arial;
    if (!Arial.loadFromFile("arial.ttf")){
        logToFile("error_log.txt", "Font Arial failed to load.");
    }
    sf::Font ArialBlack;
    if(!ArialBlack.loadFromFile("ariblk.ttf")){
        logToFile("error_log.txt", "Font Arial Black failed to load.");
    }

    sf::Text Title("GALACTIGA", ArialBlack, 64);
    Title.setPosition(200, 100);
    Title.setColor(sf::Color::Red);

    sf::Text Start("START GAME", ArialBlack, 48);
    Start.setPosition(225, 400);

    sf::Text Exit("EXIT GAME", ArialBlack, 48);
    Exit.setPosition(250, 450);
    Exit.setColor(sf::Color::Red);

    sf::Text PauseText("PAUSED!", ArialBlack, 48);
    PauseText.setPosition(275, 400);

    sf::Text CountdownText("3", ArialBlack, 64);
    CountdownText.setPosition(275, 400);

    //texture making
    sf::Texture spriteSheet;
    if(!spriteSheet.loadFromFile("spritesheet.png")){
        logToFile("error_log.txt", "Sprite sheet failed to load.");
    }

    Player playerShip = Player(spriteSheet);

    sf::Clock unpauser;
    sf::Clock frameTimer;
    sf::Clock logicTimer;
    sf::Time frameTime = sf::seconds(1.0/60.0);
    sf::Time deltaTime = sf::Time::Zero;

    //This should have the loop split into a case for each state
    while (window.isOpen())
    {
        switch(currentGameState)
        {
            case MAIN_MENU:
            {
                sf::Event event;
                while(window.pollEvent(event))
                {
                    if(event.type == sf::Event::Closed)
                        window.close();
                    if(event.type == sf::Event::KeyPressed)
                    {
                        menuAct(currentGameState, event, Start, Exit);
                        frameTimer.restart();
                    }
                }
                window.clear();
                window.draw(Title);
                window.draw(Start);
                window.draw(Exit);
                break;
            }
            case IN_GAME:
            {
                //The window is cleared at the start so we can potentially make draw calls when we want
                window.clear();
                sf::Event event;
                while(window.pollEvent(event))
                {
                    if(event.type == sf::Event::Closed)
                    {
                        window.close();
                    }
                    if((event.type == sf::Event::KeyPressed) || (event.type == sf::Event::KeyReleased))
                    {
                        if(paused == PAUSED)
                            pausedInput(event, currentGameState, paused, unpauser);
                        else if (paused == UNPAUSED)
                            inputHandler(event, playerShip, paused);
                    }
                }
                if(paused == UNPAUSED)
                {
                    //do all the updating
                    deltaTime += frameTimer.restart();
                    //debug logging
                    std::string frameInfo = std::to_string(deltaTime.asSeconds()) + " " + std::to_string(frameTimer.getElapsedTime().asSeconds()) + " " + std::to_string(frameTime.asSeconds());
                    logToFile("game_log.txt", frameInfo);
                    std::string playerInfo = std::to_string(playerShip.accel) + " " + std::to_string(playerShip.velocity);
                    logToFile("game_log.txt", playerInfo);
                    //end debug
                    while(deltaTime > frameTime)
                    {
                        gameUpdate(frameTime, playerShip);
                        deltaTime -= frameTime;
                        logToFile("game_log.txt", "UPDATED");
                    }
                } else if (paused == PAUSED) {
                    //nothing updates and we're told we're paused
                    frameTimer.restart();
                } else if (paused == UNPAUSING){
                    //Countdown from 3 to play
                    frameTimer.restart();
                    if(unpauser.getElapsedTime() >= sf::seconds(3.0)){
                        paused = UNPAUSED;
                        deltaTime == sf::Time::Zero;
                    } else if (unpauser.getElapsedTime() >= sf::seconds(2.0)) {
                        CountdownText.setString("1");
                    } else if (unpauser.getElapsedTime() >= sf::seconds(1.0)) {
                        CountdownText.setString("2");
                    } else {
                        CountdownText.setString("3");
                    }
                }
                window.draw(playerShip);

                if(paused == PAUSED)
                    window.draw(PauseText);
                if(paused == UNPAUSING)
                    window.draw(CountdownText);
                break;
            }
            case GAME_OVER:
            {
                sf::Event event;
                while(window.pollEvent(event))
                {
                    if(event.type == sf::Event::Closed)
                        window.close();
                }

                break;
            }
            case CLOSING:
            {
                window.close();
            }
        }

        window.display();

    }

    return 0;
}
