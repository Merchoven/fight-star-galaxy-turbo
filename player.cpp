#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include "player.h"


Player::Player(sf::Texture& spriteSheet)
{
    spriteState.left = 32;
    spriteState.top = 0;
    spriteState.width = 32;
    spriteState.height = 32;

    playerHitboxRear.left = 384.0;
    playerHitboxRear.top = 520.0;
    playerHitboxRear.width = 32.0;
    playerHitboxRear.height = 12.0;

    playerHitboxNose.left = 395.0;
    playerHitboxNose.top = 500.0;
    playerHitboxNose.width = 10.0;
    playerHitboxNose.height = 20.0;

    playerHitboxLeftGun.left = 384.0;
    playerHitboxLeftGun.top = 511.0;
    playerHitboxLeftGun.width = 2.0;
    playerHitboxLeftGun.height = 10.0;

    playerHitboxRightGun.left = 394.0;
    playerHitboxRightGun.top = 511.0;
    playerHitboxRightGun.width = 2.0;
    playerHitboxRightGun.height = 10.0;

    playerSprite.setTexture(spriteSheet);
    playerSprite.setTextureRect(spriteState);
    playerSprite.setPosition(384, 500);

    velocity = 0.0;
    accel = 0;
    accelRate = 60.0;

    lives = 3;

    decelerating = false;
}

void Player::update(sf::Time dT)
{
    //Putting this first will make deceleration faster
    if((accel < 0) && (velocity > -5.0)) {
        velocity += (-1 * accelRate * dT.asSeconds());
    } else if((accel > 0) && (velocity < 5.0)) {
        velocity += (accelRate * dT.asSeconds());
    }

    if(decelerating == true)
    {
        if(std::abs(velocity) < (accelRate * dT.asSeconds())){
            velocity = 0.0;
            decelerating = false;
        } else if(velocity > 0.0){
            velocity += (-1 * accelRate * dT.asSeconds());
        } else if(velocity < 0.0){
            velocity += (accelRate * dT.asSeconds());
        }
    }

    playerSprite.move(velocity, 0.0);
    playerHitboxLeftGun.left += velocity;
    playerHitboxRightGun.left += velocity;
    playerHitboxRear.left += velocity;
    playerHitboxNose.left += velocity;
}



void Player::draw(sf::RenderTarget &window, sf::RenderStates states) const
{
    window.draw(playerSprite, states);
}
