#include <fstream>
#include "logging_and_debug.h"

void logToFile(std::string filename, std::string message)
{
    std::fstream logFile;
    logFile.open(filename, std::ios::out);
    logFile << message << std::endl;
    logFile.close();
}
