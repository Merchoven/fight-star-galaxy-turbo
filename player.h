#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class Player : public sf::Drawable
{
public:
    Player(sf::Texture&);
    void update(sf::Time dT);
    sf::FloatRect playerHitboxRear;
    sf::FloatRect playerHitboxNose;
    sf::FloatRect playerHitboxLeftGun;
    sf::FloatRect playerHitboxRightGun;

    bool decelerating;
    double velocity;
    int accel;
    double accelRate;
    int lives;

private:
    sf::Sprite playerSprite;
    //The four hitbox rectangles will minimize annoyance that would come from a dumb bounding box that detects collisions that
    //didn't really happen.


    sf::IntRect spriteState;


    virtual void draw(sf::RenderTarget& window, sf::RenderStates states) const;
};

#endif // PLAYER_H_INCLUDED
