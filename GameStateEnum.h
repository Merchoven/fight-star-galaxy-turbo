#ifndef GAMESTATEENUM_H_INCLUDED
#define GAMESTATEENUM_H_INCLUDED

enum GameState
    {
        MAIN_MENU,
        IN_GAME,
        GAME_OVER,
        CLOSING
    };

#endif // GAMESTATEENUM_H_INCLUDED
