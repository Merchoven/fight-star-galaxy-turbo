#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED
#include <SFML/Graphics.hpp>

void menuAct(int &gameState, sf::Event &event, sf::Text &start, sf::Text &exit);

//these are interface things, they belong here and not in the player file
//it does mean more includes have to go into menu.cpp though, so I'm not entirely sure
void inputHandler(sf::Event input, Player& playerShip, int& paused);

void pausedInput(sf::Event input, int& gameState, int& paused, sf::Clock& unpauser);

#endif // MENU_H_INCLUDED
